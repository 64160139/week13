package com.fah.week13;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextField;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ButtonExample extends JFrame {
    JButton button;
    JButton clearBut;
    JTextField text ;
    public ButtonExample() {
        super("Button Example");
        button = new JButton("Welcome");
        text = new JTextField();
        text.setBounds(50,50,150,20);
        button.setBounds(50, 100,120, 30);
        button.setIcon(new ImageIcon("welcome.png"));
        button.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                text.setText("Welcome to Burapha");

            }
        });
        clearBut = new JButton("Clear");
        clearBut.setBounds(180,100,95,30);
        clearBut.setIcon(new ImageIcon("cross.png"));
        clearBut.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                text.setText("");
                
            }});
        this.add(clearBut);
        this.add(text);
        this.add(button);
        this.setSize(400, 400);
        this.setLayout(null);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setVisible(true);
    }

    public static void main(String[] args) {
        ButtonExample frame = new ButtonExample();
    }
}
