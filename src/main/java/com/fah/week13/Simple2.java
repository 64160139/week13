package com.fah.week13;

import javax.swing.JButton;
import javax.swing.JFrame;

class MyFrame extends JFrame {
    JButton button;

    public MyFrame() {
        super("First Frame");
        button = new JButton("Click");
        button.setBounds(130, 100, 100, 40);
        this.setLayout(null);
        this.add(button);
        this.setSize(400, 500);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    }
}

public class Simple2 {
    public static void main(String[] args) {
        MyFrame frameapp = new MyFrame();
        frameapp.setVisible(true);
    }
}
